<?php
declare (strict_types=1);

namespace mark\wechat\notice;

use mark\system\Os;

/**
 * Class Notice
 *
 * @description  微信模板消息
 * @package      mark\wechat\notice
 * @link         https://developers.weixin.qq.com/doc/offiaccount/Message_Management/Template_Message_Interface.html
 */
final class Notice {

    /**
     * Notice constructor.
     */
    private function __construct() { }

    /**
     * 系统报警通知
     *
     * @param string $wxid
     * @param string $first
     * @param string $source
     * @param int    $time
     * @param string $content
     * @param string $url
     * @param string $remark
     *
     * @return array
     */
    public static function runevent_msg(string $wxid, string $first = '', string $source = '', $time = 0, $content = '', $url = '', $remark = ''): array {
        if (empty($first)) {
            $first = '运行预警';
        }
        if (empty($source)) {
            $source = '无效频道';
        }
        if (empty($time) || !is_numeric($time)) {
            $time = time();
        }
        if (empty($content)) {
            $content = implode(' ', Os::getOs()) . ' ' . implode(' ', Os::getBrand()) . ' ' . implode(' ', Os::getBrowser());
        }

        if (empty($remark)) {
            $ipinfo = \mark\auth\tool\Client::getIpInfo('access_token:temp_token:' . $wxid, Os::getIpvs())->toArray();
            $remark = implode('_', array_filter([$ipinfo['country'] ?? '', $ipinfo['province'] ?? '', $ipinfo['city'] ?? '', $ipinfo['isp'] ?? '']));

            $content .= $remark;
        }

        return array('touser' => $wxid,
                     'template_id' => 'RTgP0JuE-OecgnurtQ4fVPL8vUNurPW5jubkKZpWTio',
                     'url' => $url,
                     'topcolor' => '#FF0000',
                     'data' => array('first' => array('value' => $first, 'color' => '#DC3545'),
                                     'keyword1' => array('value' => $source, 'color' => '#FD7E14'),
                                     'keyword2' => array('value' => date('Y-m-d H:i:s', $time)),
                                     'keyword3' => array('value' => $content, 'color' => '#343A40'),
                                     'remark' => array('value' => $remark)),
                     'chattype' => '运行报警'
        );

    }

    /**
     * 申请加入通知
     *
     * @param string $wxid
     * @param string $first
     * @param string $student
     * @param int    $time
     * @param string $url
     * @param string $remark
     *
     * @return array
     */
    public static function apply_msg(string $wxid, string $first = '', string $student = '', $time = 0, $url = '', $remark = ''): array {
        if (empty($first)) {
            $first = '有新成员申请加入，请审核';
        }
        if (empty($student)) {
            $student = '匿名学员';
        }
        if (empty($time) || !is_numeric($time)) {
            $time = time();
        }

        if (empty($remark)) {
            $remark = '通知备注';
        }

        return array('touser' => $wxid,
                     'template_id' => 'f-FxiijZ87mPt68zYiiZHOi8XaQa-wOh4FiBWWDtq18',
                     'url' => $url,
                     'topcolor' => '#FF0000',
                     'data' => array('first' => array('value' => $first, 'color' => '#DC3545'),
                                     'keyword1' => array('value' => $student, 'color' => '#FD7E14'),
                                     'keyword2' => array('value' => date('Y-m-d H:i:s', $time)),
                                     'remark' => array('value' => $remark)),
                     'chattype' => '申请加入'
        );

    }

    /**
     * 审核结果通知
     *
     * @param string $wxid
     * @param string $first
     * @param string $subject
     * @param int    $time
     * @param string $result
     * @param string $opinion
     * @param string $url
     * @param string $remark
     *
     * @return array
     */
    public static function audit_msg(string $wxid, string $first = '', string $subject = '', $time = 0, $result = '', $opinion = '', $url = '', $remark = ''): array {
        if (empty($first)) {
            $first = '“项目”已通过审核，登录平台解锁更多操作！';
        }
        if (empty($subject)) {
            $subject = '审查主题';
        }
        if (empty($time) || !is_numeric($time)) {
            $time = time();
        }
        if (empty($result)) {
            $result = '审核结果';
        }
        if (empty($opinion)) {
            $opinion = '审核意见';
        }

        if (empty($remark)) {
            $remark = '点击查看详情，感谢你使用天府软件！';
        }
        return array('touser' => $wxid,
                     'template_id' => 'QqkeEA_lTQediZyOyDzHP9Q0Q9_NU0cMSMPj7N8huLI',
                     'url' => $url,
                     'topcolor' => '#FF0000',
                     'data' => array('first' => array('value' => $first, 'color' => '#DC3545'),
                                     'keyword1' => array('value' => $subject, 'color' => '#FD7E14'),
                                     'keyword2' => array('value' => date('Y-m-d H:i:s', $time)),
                                     'keyword3' => array('value' => $result, 'color' => '#343A40'),
                                     'keyword4' => array('value' => $opinion, 'color' => '#343A40'),
                                     'remark' => array('value' => $remark)),
                     'chattype' => '审核结果'
        );
    }

    /**
     * 营业情况明细通知
     *
     * @param string $wxid
     * @param string $first
     * @param string $time
     * @param string $content
     * @param string $remark
     * @param string $url
     * @param string $color
     *
     * @return array
     */
    public static function operate_report(string $wxid, $first = '', $time = '', $content = '', $remark = '', $url = '', $color = '#FD7E14'): array {
        if (empty($first)) {
            $first = '运营报告';
        }

        if (empty($time)) {
            $time = time();
        }

        if (empty($content)) {
            $content = '营业简报';
        }

        if (empty($remark)) {
            $remark = '点击查看详情，感谢使用天府物业软件';
        }

        return array(
            'touser' => $wxid,
            'template_id' => 'iOxt18T95rgsjqUilQeKsZdOG_6-6ETu9zVxhIsDeJ8',
            'url' => $url,
            'topcolor' => '#FF0000',
            'data' => array(
                'first' => array('value' => $first, 'color' => '#DC3545'),
                'keyword1' => array('value' => date('Y-m-d H:i:s', $time), 'color' => '#343A40'),
                'keyword2' => array('value' => $content, 'color' => $color),
                'remark' => array('value' => $remark, 'color' => '#5470c6')
            ),
            'chattype' => '物业日报');
    }

    /**
     * 住户审核通知
     *
     * @param string $wxid
     * @param string $first
     * @param string $name
     * @param string $address
     * @param string $url
     * @param string $remark
     *
     * @return array
     */
    public static function resident_review(string $wxid, string $first, string $name, string $address, $url = '', $remark = ''): array {
        if (empty($first)) {
            $first = '亲爱的' . $name . '： 有住户申请绑定到你的房屋，请确认并审核。申请人信息如下：';
        }
        if (empty($name)) {
            $name = '佚名大侠';
        }
        if (empty($address)) {
            $address = '无效地址';
        }

        if (empty($remark)) {
            $remark = '点击查看详情，感谢你使用天府软件！';
        }
        return array(
            'touser' => $wxid,
            'template_id' => 'DG5C90bMCpJj7K2lXJM7yrmfyAtUmFSly-yrBlwZa4k',
            'url' => $url,
            'topcolor' => '#FF0000',
            'data' => array(
                'first' => array('value' => $first, 'color' => '#DC3545'),
                'keyword1' => array('value' => $name, 'color' => '#FD7E14'),
                'keyword2' => array('value' => $address, 'color' => '#343A40'),
                'remark' => array('value' => $remark, 'color' => '#5470c6')
            ),
            'chattype' => '住户审核通知'
        );
    }

}