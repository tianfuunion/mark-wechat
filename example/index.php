<html>
<head>
    <meta http-equiv="content-type" content="text/html;charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>微信支付样例</title>
    <style type="text/css">
        ul{margin-left:10px;margin-right:10px;margin-top:10px;padding:0;}
        li{width:32%;float:left;padding:0;height:100px;display:inline;line-height:100px;color:white;font-size:x-large;word-break:break-all;word-wrap:break-word;margin:0 0 5px 1%;}
        a{-webkit-tap-highlight-color:rgba(0, 0, 0, 0);text-decoration:none;color:white;}
        a:link{-webkit-tap-highlight-color:rgba(0, 0, 0, 0);text-decoration:none;color:white;}
        a:visited{-webkit-tap-highlight-color:rgba(0, 0, 0, 0);text-decoration:none;color:white;}
        a:hover{-webkit-tap-highlight-color:rgba(0, 0, 0, 0);text-decoration:none;color:white;}
        a:active{-webkit-tap-highlight-color:rgba(0, 0, 0, 0);text-decoration:none;color:white;}
    </style>
</head>
<body>
<div align="center">
    <ul>
        <li style="background-color:#FF7F24"><a href="./jsapi.php">JSAPI支付</a></li>
        <li style="background-color:#698B22"><a href="./micropay.php">刷卡支付</a></li>
        <li style="background-color:#8B6914"><a href="./native.php">扫码支付</a></li>
        <li style="background-color:#CDCD00"><a href="./orderquery.php">订单查询</a></li>
        <li style="background-color:#CD3278"><a href="./refund.php">订单退款</a></li>
        <li style="background-color:#848484"><a href="./refundquery.php">退款查询</a></li>
        <li style="background-color:#8EE5EE"><a href="./download.php">下载订单</a></li>
    </ul>
</div>
</body>
</html>